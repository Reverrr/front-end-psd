const messages = document.getElementById('messages');
const textarea = document.getElementById('textarea');
const sendMessage = document.getElementById('send-message');

document.addEventListener("DOMContentLoaded", function(event) {
    document.addEventListener('keydown', function(event) {
        if(event.ctrlKey && event.keyCode === 13){
            addMessagesBeforeEnd(messages, textarea);
        }
    });

    sendMessage.addEventListener('click', function(event) {
        addMessagesBeforeEnd(messages, textarea);
    });
});

function addMessagesBeforeEnd(element, text) {
    element.insertAdjacentHTML('beforeend', `
            <div class="message">
                <div class="header">
                    <h2>Unknown</h2>
                    <span class="date">Unknown</span>
                </div>
                <div class="message-text">
                    <div class="border-triangle">
                        <div class="triangle"></div>
                    </div>
                    <p>${text.value}</p>
                </div>
            </div>`);
    text.value = "";
}